export function setListingData(params) {
    return {
        type: 'SET_LISTING_DATA',
        payload: params
    }
}

export function setAuthData(params) {
    return {
        type: 'SET_AUTH_DATA',
        payload: params
    }
}

export function setUserData(params) {
    return {
        type: 'SET_USER_DATA',
        payload: params
    }
}

export function setUserScheduleData(params) {
    return {
        type: 'SET_USER_SCHEDULE_DATA',
        payload: params
    }
}

export function setContractorCalendarData(params) {
    return {
        type: 'SET_CONTRACTOR_CALENDAR_DATA',
        payload: params
    }
}

export function setScheduleFormData(params) {
    return {
        type: 'SET_SCHEDULE_FORM_DATA',
        payload: params
    }
}

export function setScheduleListData(params) {
    return {
        type: 'SET_SCHEDULE_LIST_DATA',
        payload: params
    }
}

export function setAppointmentListData(params) {
    return {
        type: 'SET_APPOINTMENT_LIST_DATA',
        payload: params
    }
}
