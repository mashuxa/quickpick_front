import React from 'react';

function Terms() {
    return (
        <div className='terms-page'>
            <h1>Terms</h1>
            <p>Suspendisse facilisis augue id risus malesuada, ac dapibus orci posuere. Pellentesque pellentesque, felis
                ac elementum pretium, odio neque congue ex, quis dictum est purus sed tellus. Proin viverra purus id
                sapien varius, sit amet vestibulum augue fermentum. Suspendisse rutrum ex est, nec mollis quam rutrum
                vitae. Mauris sagittis tincidunt pellentesque. Aliquam vitae dignissim nunc, eget sodales sapien. Proin
                at erat ligula. In sit amet congue lectus.
            </p>
            <p>Donec lectus odio, interdum non iaculis nec, convallis et odio. Lorem ipsum dolor sit amet, consectetur
                adipiscing elit. Nam mauris ipsum, maximus eu leo a, maximus sodales lorem. Aenean fermentum metus a
                ante bibendum ultrices. Donec feugiat vitae libero a facilisis. Cras accumsan vehicula nisl eu
                imperdiet. Vivamus commodo orci non nibh vehicula posuere.
            </p>
            <p>Quisque cursus mi id metus molestie rutrum aliquet consectetur felis. Curabitur et posuere lectus. Sed
                dapibus dictum hendrerit. Praesent ultrices metus sed nunc auctor lacinia. Duis ultrices nisi at
                fermentum aliquam. Etiam maximus volutpat mi at ullamcorper. Nullam aliquet felis elit, non egestas sem
                finibus non. Aliquam erat volutpat. Fusce leo augue, lobortis ac blandit ut, maximus sed eros. Curabitur
                vehicula rhoncus eros et gravida. Proin malesuada sodales ligula, nec fringilla quam varius ac.
                Vestibulum sed mi sit amet quam eleifend condimentum et ut urna. Vestibulum sed interdum nunc, nec
                auctor magna. Morbi a odio lorem. Proin ultrices lorem ac turpis rutrum, eu auctor lacus congue.
            </p>
            <p>Proin accumsan eget nunc quis iaculis. Donec sagittis risus a arcu accumsan, vel viverra erat volutpat.
                Nunc maximus felis nibh, sit amet cursus lorem interdum id. Pellentesque habitant morbi tristique
                senectus et netus et malesuada fames ac turpis egestas. Praesent et interdum lorem. Suspendisse sit amet
                feugiat lectus. Proin sed semper orci, eu facilisis orci. Vivamus condimentum rutrum dignissim. Etiam a
                bibendum quam. Sed sed viverra ex. Nam accumsan massa et felis tristique dictum.
            </p>
            <p>Suspendisse convallis eu nunc quis volutpat. Maecenas in sodales urna, non volutpat metus. Nulla in
                turpis rhoncus, tincidunt eros quis, finibus turpis. Ut dignissim sapien nec sapien auctor, in rhoncus
                mi elementum. Aenean vitae vehicula tellus. Sed ullamcorper ullamcorper leo. Nulla ut diam nec sem
                ullamcorper cursus finibus a dui. Nullam vel aliquam ex, nec scelerisque nibh. Duis rhoncus neque vel
                diam finibus mattis ac at diam. Suspendisse potenti. Sed id diam urna. Pellentesque enim justo, tempor
                vitae ex sit amet, blandit vestibulum lacus. Nunc hendrerit justo vel augue fringilla dignissim.
            </p>
        </div>

    );
}


export {Terms};
