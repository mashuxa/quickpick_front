import React from 'react';


function Policy() {
    return (
        <div className='policy-page'>
            <h1>Policy</h1>
            <p>Nullam nec dictum risus. Duis faucibus leo ornare, tempor arcu in, bibendum dolor. Phasellus et ligula
                dui. Nullam quis consectetur nulla, ut venenatis ligula. Sed ac viverra mauris. Vivamus pretium
                dignissim ullamcorper. Aliquam mattis, erat id imperdiet fermentum, leo nisl dapibus ante, vel fringilla
                sem mi eget sem. Fusce tincidunt ut magna a viverra. Ut dolor ligula, ultrices non dignissim ac, sodales
                non risus. Suspendisse sit amet lacus quis sapien sodales placerat quis malesuada magna. In tincidunt
                sollicitudin diam, nec lobortis tortor ultricies nec. Mauris nec sagittis quam. Aliquam erat volutpat.
            </p>
            <p>Integer gravida urna vitae purus bibendum consectetur. Integer dictum semper tortor vitae interdum. Nunc
                leo dolor, maximus et est eget, dictum hendrerit mauris. Suspendisse potenti. Nam augue tortor,
                tristique nec neque eu, feugiat elementum odio. Aenean non nunc tempus urna facilisis ultricies et vitae
                tortor. Ut pellentesque lacus nec lectus accumsan semper.
            </p>
            <p>Curabitur egestas nisi sed metus posuere ultrices. Praesent pulvinar quam sit amet orci pellentesque, non
                laoreet nulla finibus. Vestibulum ut euismod felis, at lobortis urna. Class aptent taciti sociosqu ad
                litora torquent per conubia nostra, per inceptos himenaeos. Praesent a tellus eget purus gravida
                facilisis eu at odio. Maecenas ante sem, pretium eu faucibus ornare, pretium id urna. Nullam nisi
                turpis, tincidunt sit amet elementum et, pretium a justo. Morbi tempor metus vel libero mollis pulvinar.
                Nulla dui diam, dapibus eu eleifend a, facilisis sit amet enim. Ut tellus nunc, porttitor eu eros in,
                malesuada blandit orci.
            </p>
            <p>Aenean dapibus elementum erat non eleifend. Nullam dapibus risus nec eros dapibus, congue varius tellus
                sodales. Phasellus ac lectus aliquet, condimentum dolor ac, molestie elit. Morbi commodo ipsum vitae
                blandit fermentum. Vivamus eget felis est. Maecenas blandit, diam ac ornare consequat, ex turpis
                tincidunt nibh, vel fringilla libero justo id orci. Maecenas id ullamcorper turpis, nec rutrum quam.
                Cras in lobortis nisl. In ullamcorper lacus nibh, id rhoncus magna pharetra quis. Nulla vel ante vitae
                lectus efficitur egestas. Phasellus blandit consectetur ex, vel euismod lectus ultrices sed. Donec eget
                risus ipsum. Aenean nec sodales dui. Ut vitae volutpat justo.
            </p>
            <p>Nunc ultrices nulla a orci ullamcorper, sed ullamcorper orci ultrices. Sed tempus a ligula et euismod.
                Nullam ut cursus metus. Phasellus hendrerit congue odio, finibus blandit turpis semper id. Nulla quis
                ullamcorper metus. Curabitur magna nisi, dictum ut lacinia ut, mollis ac dolor. Aliquam sapien magna,
                sollicitudin vel mollis vel, elementum elementum justo. Donec aliquet sollicitudin purus eget
                vestibulum. Vivamus at leo ac sapien blandit elementum. Ut et metus consequat, mattis tortor id,
                elementum felis. In maximus aliquam odio eu gravida.
            </p>
        </div>

    );
}

export {Policy};
